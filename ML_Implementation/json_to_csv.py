# A tool for making csv's out of json files

import json

def translate(tier):
    # If tier = all then we make a big csv for all the divisions
    if tier == 'all':
        tiers = ['bronze', 'silver', 'gold', 'platinum', 'diamond', 'master', 'challenger']
    else:
        tiers = [tier]

    # Making an empty csv string
    csv = "top_cs1,top_vscore1,top_dto1,jg_cs1,jg_vscore1,jg_dto1,mid_cs1,mid_vscore1,mid_dto1,adc_cs1,adc_vscore1,adc_dto1,supp_cs1,supp_vscore1,supp_dto1," \
          "top_cs2,top_vscore2,top_dto2,jg_cs2,jg_vscore2,jg_dto2,mid_cs2,mid_vscore2,mid_dto2,adc_cs2,adc_vscore2,adc_dto2,supp_cs2,supp_vscore2,supp_dto2,winner,matchId\n"

    # # Getting the champion list
    # champions_file = open('../Data Retrieval/lol_champs_list.json')
    # champions = json.load(champions_file)
    # champions_file.close()

    # actual making of csv out of json
    for t in tiers:

        t_games_file = open('../Data Retrieval/' + t + ' games/' + t + '_remake_filtered_games.json')
        t_games = json.load(t_games_file)
        t_games_file.close()

        for game in t_games:
            # print('\n' + str(t_games[game]['gameId']))
            # print(t_games[game]['gameCreation'])
            # print(t_games[game]['gameDuration'])
            # print(t_games[game]['gameType'] + '\n')

            # Preparing a line for each game
            game_line = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            # Writing gameId
            game_line[31] = t_games[game]['gameId']

            # Extracting the label - who won
            for team in t_games[game]['teams']:
                if team['win'] == 'Win':
                    game_line[30] = team['teamId'] / 100

            for p in t_games[game]['participants']:
                # print('team: ' + str(p['teamId']) + ',\tlane: ' + p['timeline']['lane'] + ',\trole: ' + p['timeline']['role'] + '\tchampion: ' + champions[str(p['championId'])]['name'])

                game_line[(p['participantId'] - 1) * 3] = p['stats']['totalMinionsKilled']  # inserting cs
                game_line[(p['participantId'] - 1) * 3 + 1] = p['stats']['visionScore']  #
                game_line[(p['participantId'] - 1) * 3 + 2] = p['stats']['damageDealtToObjectives']  #




            csv += reduce((lambda x, y: str(x) + ',' + str(y)), game_line) + '\n'




    # writing the csv string to a file
    if tier == 'all':
        csv_file = open("all_games_csv.txt", "w")
        csv_file.write(csv)
        csv_file.close()
    else:
        csv_file = open(tier + "_games_csv.txt", "w")
        csv_file.write(csv)
        csv_file.close()

translate('bronze')
translate('silver')
translate('gold')
translate('platinum')
translate('diamond')
translate('master')
translate('challenger')
translate('all')