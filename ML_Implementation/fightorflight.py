# this is a win chance analyzer for league of legends games
import numpy as np

# arguments and other system stuff
import sys
import time

# api stuff
import json
import urllib2


# =============================================================================================================================================
# =============================================================================================================================================
# ============================================================ Program start point ============================================================
# =============================================================================================================================================
# =============================================================================================================================================

# ============================================================== Args retrieval ===============================================================
# Get the summoner names of the 4 teammates from command line
# Throw an error if input is unexpected. TODO: what is unexpected?
# Example: >python fightorflight.py "hahybg" "Its Me Mario" "xarry26" "Holy Bonobo"
summonerNames = sys.argv[1:]
summonerNamesLen = len(summonerNames)
if(summonerNamesLen != 4):
	print("Unexpected input - Please insert 4 valid summoner names")
	print("Expected input: \"sum1Name\" " +
						  "\"sum2Name\" " +
						  "\"sum3Name\" " +
						  "\"sum4Name\" ")
	exit()

# TODO: Consider removing those printouts in the future
print(summonerNames)
print(summonerNamesLen)


# ==================================================== JSON objects retrieval from the api ====================================================
# My developer api key. TODO: figure out how to acquire it dynamically.
api_key = 'RGAPI-7db48f70-585b-43e6-8e9c-5998b7412c16'

# Transforming the summoner names to url strings (%20 instead of space etc etc)
summonerNames = [urllib2.quote(i) for i in summonerNames]

# TODO: Consider removing those printouts in the future
print(summonerNames)

# Retrieving JSON objects for summoners - we will use the info provided to retrieve more objects
summonersByNameJSON = [json.load(urllib2.urlopen('https://eun1.api.riotgames.com/lol/summoner/v3/summoners/by-name/' +
									 summonerNames[i] +
									 '?api_key='+
									 api_key))
					   for i in range(summonerNamesLen)]

# TODO: Consider removing those printouts in the future
for i in range(summonerNamesLen):
	print(summonersByNameJSON[i])

# Retrieving JSON objects for games per summoner id
matchesByAccountIdJSON = [json.load(urllib2.urlopen('https://eun1.api.riotgames.com/lol/match/v3/matchlists/by-account/' +
									 str(summonersByNameJSON[i]['accountId']) +
									 '?api_key='+
									 api_key))
					   	  for i in range(summonerNamesLen)]

# TODO: Consider removing those printouts in the future
for i in range(summonerNamesLen):
	print(matchesByAccountIdJSON[i])