# This script will predict the winner of a match using ML algorithm

import numpy as np
from sklearn import preprocessing, model_selection, neighbors, svm
import pandas as pd
from sklearn.linear_model import LinearRegression

import json
import urllib2

def train_by_tier(tier_path):
    print("[DEBUG] Prediction based on " + tier_path)
    df = pd.read_csv(tier_path)
    df.drop(['matchId'], 1, inplace=True)

    X = np.array(df.drop(['winner'], 1))
    y = np.array(df['winner'])

    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2)

    # K neighbors - good for true/false labels
    clf = neighbors.KNeighborsClassifier()
    clf.fit(X_train, y_train)

    # # Linear regression - good for numeric labels
    # clf = LinearRegression(n_jobs = -1)
    # clf.fit(X_train, y_train)

    # # SVM - works very bad. ~50% accuracy
    # clf = svm.SVC()
    # clf.fit(X_train, y_train)

    accuracy = clf.score(X_test, y_test)
    print("[DEBUG] " + str(accuracy))

    return clf

# Fails on this game: 2018470974
def predict_by_gameId(clf, gameId):
    # For online testing:
    # game_obj = json.load(urllib2.urlopen('https://eun1.api.riotgames.com/lol/match/v3/matches/' +
    #                                    str(gameId) +
    #                                    '?api_key=RGAPI-5e6b1edf-4185-45aa-af40-c0370f3ba09f'))

    # For offline testing:
    js_file = open("../Data Retrieval/example.json")
    game_obj = json.load(js_file)

    example_measures2 = np.array([(p['stats']['totalMinionsKilled'], p['stats']['visionScore'], p['stats']['damageDealtToObjectives']) for p in game_obj['participants']])
    example_measures2 = example_measures2.reshape(1, -1)
    # print(str(example_measures2))

    # example_measures = np.array([[6,49,102,227,8,11067,224,20,3074,30,24,5081,198,15,3901,125,12,7066,20,29,12846,195,26,9395,196,11,18303,53,46,4137], [246,24,18650,246,37,12795,160,20,5414,56,35,29371,75,88,2816,295,44,5183,202,10,0,197,19,1348,18,72,854,44,32,9993]])
    # example_measures = example_measures.reshape(len(example_measures), -1)
    # print(str(example_measures))

    prediction = clf.predict(example_measures2)
    print("[DEBUG] Prediction: " + str(prediction[0]))

    actual_outcome =  1 if game_obj['teams'][0]['win'] == 'Win' else 2
    print("[DEBUG] Actual outcome: " + str(actual_outcome))

    return prediction[0], actual_outcome