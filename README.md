**FightOrFlight**

Winner prediction app based on League of Legends game statistics

---

# Overview

This app uses machine learning tools to predict the outcome of an online PVP game (win or loss) based on game statistics.
It has a gui built with Tkinter and uses K-neighbors to make its predictions.

---

# Getting started

Here are listed requirements and steps that need to be done for the app to run

## Requirements
1. Python2.7
2. Libraries - Tkinter, numpy, sklearn, pandas, and json.

## Install and run steps
1. Clone the project.
2. In ML_Implementation/gui.py change line 4 to sys.path.insert(0, r'<path-to-ML_Implementation>').
3. Cd in the terminal to the GUI folder of the project.
4. Run 'python gui.py'.