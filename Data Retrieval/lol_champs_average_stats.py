# lol champions average stats
# this module builds averages statistics per champion and stores it in a json file


# api stuff
import json
import urllib2
import os
import time
import threading


# ======================================================================================================================
# ======================================================================================================================
# ===================================================== Constants ======================================================
# ======================================================================================================================
# ======================================================================================================================

# Rate limits
REQUESTS_PER_SECOND_LIMIT = 10
REQUESTS_PER_MINUTE_LIMIT = 40

# My developer api key. TODO: figure out how to acquire it dynamically.
API_KEY = 'RGAPI-2350fa01-1a80-48f9-8998-1fedb553993a'

# Query params:
API_METHOD = 'https://'
API_REGION = 'eun1.'


# ======================================================================================================================
# ======================================================================================================================
# ===================================================== Constants ======================================================
# ======================================================================================================================
# ======================================================================================================================


# ======================================================================================================================
# ======================================================================================================================
# ===================================================== Functions ======================================================
# ======================================================================================================================
# ======================================================================================================================

# Retrieve JSON object from a file
def get_json_from_file(path):
    js_file = open(path)
    js_obj = json.load(js_file)
    js_file.close()
    return js_obj


# Retrieve JSON object for the champions list from the api
# Might be obsolete when we have an empty champion list JSON file
def get_json_from_api():

    # Query params:
    api_base_query = 'api.riotgames.com/lol/static-data/v3/champions'
    api_locale = 'en_US'
    api_champListData = 'keys'
    api_tags = 'keys'
    api_dataById = 'true'

    # Retrieving a JSON object for champions from the api
    js_obj = json.load(urllib2.urlopen(API_METHOD +
                                       API_REGION +
                                       api_base_query +
                                       '?locale=' + api_locale +
                                       '&champListData=' + api_champListData +
                                       '&tags=' + api_tags +
                                       '&dataById=' + api_dataById +
                                       '&api_key=' + API_KEY))
    js_obj = js_obj['data']
    return js_obj


# Adding new keys to the JSON object
# Might be obsolete when we have the augmented JSON
def augment_champions_average_stats(championAverageStatsById):
    for champID in championAverageStatsById:
        # Games played
        championAverageStatsById[champID]["~00games_played"] = 0

        ################################### Total stats #################################
        # Win/Lose stats
        championAverageStatsById[champID]["~01total_won"] = 0
        championAverageStatsById[champID]["~02total_lost"] = 0

        # KDA stats
        championAverageStatsById[champID]["~03total_kills"] = 0
        championAverageStatsById[champID]["~04total_deaths"] = 0
        championAverageStatsById[champID]["~05total_assists"] = 0

        # Damage dealt to champions stats
        championAverageStatsById[champID]["~06total_totalDamageDealtToChampions"] = 0
        championAverageStatsById[champID]["~07total_magicDamageDealtToChampions"] = 0
        championAverageStatsById[champID]["~08total_physicalDamageDealtToChampions"] = 0
        championAverageStatsById[champID]["~09total_trueDamageDealtToChampions"] = 0

        # Damage dealt
        championAverageStatsById[champID]["~10total_totalDamageDealt"] = 0
        championAverageStatsById[champID]["~11total_magicDamageDealt"] = 0
        championAverageStatsById[champID]["~12total_physicalDamageDealt"] = 0
        championAverageStatsById[champID]["~13total_trueDamageDealt"] = 0

        championAverageStatsById[champID]["~14total_damageDealtToObjectives"] = 0
        championAverageStatsById[champID]["~15total_damageDealtToTurrets"] = 0

        # Damage taken and healed
        championAverageStatsById[champID]["~16total_totalHeal"] = 0
        championAverageStatsById[champID]["~17total_totalDamageTaken"] = 0
        championAverageStatsById[champID]["~18total_magicalDamageTaken"] = 0
        championAverageStatsById[champID]["~19total_physicalDamageTaken"] = 0
        championAverageStatsById[champID]["~20total_trueDamageTaken"] = 0
        championAverageStatsById[champID]["~21total_damageSelfMitigated"] = 0

        # Income
        championAverageStatsById[champID]["~22total_goldEarned"] = 0
        championAverageStatsById[champID]["~23total_goldSpent"] = 0

        # Vision
        championAverageStatsById[champID]["~24total_visionScore"] = 0
        championAverageStatsById[champID]["~25total_visionWardsBoughtInGame"] = 0
        championAverageStatsById[champID]["~26total_wardsPlaced"] = 0
        championAverageStatsById[champID]["~27total_wardsKilled"] = 0

        # Neutral monsters and minions
        championAverageStatsById[champID]["~28total_totalMinionsKilled"] = 0
        championAverageStatsById[champID]["~29total_neutralMinionsKilled"] = 0
        championAverageStatsById[champID]["~30total_neutralMinionsKilledTeamJungle"] = 0
        championAverageStatsById[champID]["~31total_neutralMinionsKilledEnemyJungle"] = 0

        # Firsts
        championAverageStatsById[champID]["~32total_firstBloodKill"] = 0
        championAverageStatsById[champID]["~33total_firstBloodAssist"] = 0
        championAverageStatsById[champID]["~34total_firstTowerKill"] = 0
        championAverageStatsById[champID]["~35total_firstTowerAssist"] = 0
        championAverageStatsById[champID]["~36total_firstInhibitorKill"] = 0
        championAverageStatsById[champID]["~37total_firstInhibitorAssist"] = 0

        # Objective kills and Crowd Control
        championAverageStatsById[champID]["~38total_turretKills"] = 0
        championAverageStatsById[champID]["~39total_inhibitorKills"] = 0

        championAverageStatsById[champID]["~40total_timeCCingOthers"] = 0  # is actually cc score
        championAverageStatsById[champID]["~41total_totalTimeCrowdControlDealt"] = 0

        ################################### Average stats ###############################
        # Win/Lose stats
        championAverageStatsById[champID]["~42average_won"] = 0
        championAverageStatsById[champID]["~43average_lost"] = 0

        # KDA stats
        championAverageStatsById[champID]["~44average_kills"] = 0
        championAverageStatsById[champID]["~45average_deaths"] = 0
        championAverageStatsById[champID]["~46average_assists"] = 0

        # Damage dealt to champions stats
        championAverageStatsById[champID]["~47average_totalDamageDealtToChampions"] = 0
        championAverageStatsById[champID]["~48average_magicDamageDealtToChampions"] = 0
        championAverageStatsById[champID]["~49average_physicalDamageDealtToChampions"] = 0
        championAverageStatsById[champID]["~50average_trueDamageDealtToChampions"] = 0

        # Damage dealt
        championAverageStatsById[champID]["~51average_totalDamageDealt"] = 0
        championAverageStatsById[champID]["~52average_magicDamageDealt"] = 0
        championAverageStatsById[champID]["~53average_physicalDamageDealt"] = 0
        championAverageStatsById[champID]["~54average_trueDamageDealt"] = 0

        championAverageStatsById[champID]["~55average_damageDealtToObjectives"] = 0
        championAverageStatsById[champID]["~56average_damageDealtToTurrets"] = 0

        # Damage taken and healed
        championAverageStatsById[champID]["~57average_totalHeal"] = 0
        championAverageStatsById[champID]["~58average_totalDamageTaken"] = 0
        championAverageStatsById[champID]["~59average_magicalDamageTaken"] = 0
        championAverageStatsById[champID]["~60average_physicalDamageTaken"] = 0
        championAverageStatsById[champID]["~61average_trueDamageTaken"] = 0
        championAverageStatsById[champID]["~62average_damageSelfMitigated"] = 0

        # Income
        championAverageStatsById[champID]["~63average_goldEarned"] = 0
        championAverageStatsById[champID]["~64average_goldSpent"] = 0

        # Vision
        championAverageStatsById[champID]["~65average_visionScore"] = 0
        championAverageStatsById[champID]["~66average_visionWardsBoughtInGame"] = 0
        championAverageStatsById[champID]["~67average_wardsPlaced"] = 0
        championAverageStatsById[champID]["~68average_wardsKilled"] = 0

        # Neutral monsters and minions
        championAverageStatsById[champID]["~69average_totalMinionsKilled"] = 0
        championAverageStatsById[champID]["~70average_neutralMinionsKilled"] = 0
        championAverageStatsById[champID]["~71average_neutralMinionsKilledTeamJungle"] = 0
        championAverageStatsById[champID]["~72average_neutralMinionsKilledEnemyJungle"] = 0

        # Firsts
        championAverageStatsById[champID]["~73average_firstBloodKill"] = 0
        championAverageStatsById[champID]["~74average_firstBloodAssist"] = 0
        championAverageStatsById[champID]["~75average_firstTowerKill"] = 0
        championAverageStatsById[champID]["~76average_firstTowerAssist"] = 0
        championAverageStatsById[champID]["~77average_firstInhibitorKill"] = 0
        championAverageStatsById[champID]["~78average_firstInhibitorAssist"] = 0

        # Objective kills and Crowd Control
        championAverageStatsById[champID]["~79average_turretKills"] = 0
        championAverageStatsById[champID]["~80average_inhibitorKills"] = 0

        championAverageStatsById[champID]["~81average_timeCCingOthers"] = 0  # is actually cc score
        championAverageStatsById[champID]["~82average_totalTimeCrowdControlDealt"] = 0


# Write JSON dictionary object to disk
def write_json_dict_to_disk(json_dict_object, json_file_name):
    # Converting the JSON object to a numerically ordered string by keys
    json_dict_object_as_string = json.dumps({int(x): json_dict_object[x] for x in json_dict_object.keys()},
                                            sort_keys=True, indent=4)

    # Writing the augmented JSON to a file
    json_file = open(json_file_name, 'wb')
    json_file.write(json_dict_object_as_string)
    json_file.close()


# Create empty JSON file with average stats for champions from file
def create_empty_champion_average_stats_from_file(path):
    champions_json = get_json_from_file(path)
    augment_champions_average_stats(champions_json)
    write_json_dict_to_disk(champions_json, "lol_champs_average_stats_empty.json")


# Create empty JSON file with average stats for champions from api
def create_empty_champion_average_stats_from_api():
    champions_json = get_json_from_api()
    augment_champions_average_stats(champions_json)
    write_json_dict_to_disk(champions_json, "lol_champs_average_stats_empty.json")


# Given a tier, calculate champion average stats for this tier
def fill_champions_avrage_stats(tier):

    # arguments check
    if tier not in ['bronze', 'silver', 'gold', 'platinum', 'diamond', 'master', 'challenger', 'all']:
        print('[ERROR] Unexpected tier name.\n\tTier name should be one of the following:\n' +
              '\tbronze, silver, gold, platinum, diamond, master, challenger, all\n')
        return

    # How to do it:
    # 1. Get 10 players per league (division 3 of each league)
    # 2. Get their 100 games
    # 3. Fill in details

    # Initializing a new empty champion stats file for this tier
    lol_champs_average_stats = get_json_from_file("lol_champs_average_stats_empty.json")

    # Initializing a lock for synchronization purposes
    lock = threading.Lock()

    # If tier = all then we aggregate games from all divisions
    if tier == 'all':
        tiers = ['bronze', 'silver', 'gold', 'platinum', 'diamond', 'master', 'challenger']
    else:
        tiers = [tier]

    for t in tiers:
        # Number of games checked
        games_checked = 0

        # Opening games json for this tier
        games = get_json_from_file(t + ' games/' + t + '_remake_filtered_games.json')

        # Collecting data from every game in the tier games
        for game in games:
            games_checked += 1
            for participant in games[game]['participants']:
                # print(participant['participantId'])
                # print(participant['championId'])
                # print(lol_champs_average_stats[str(participant['championId'])])

                curr = lol_champs_average_stats[str(participant['championId'])]

                ################################### Total stats #################################
                # Games played
                lock.acquire()
                prev_games_played = curr["~00games_played"]
                curr["~00games_played"] += 1
                lock.release()

                # Win/Lose stats
                curr["~01total_won"] += int(participant['stats']['win'])
                curr["~02total_lost"] += int(not participant['stats']['win'])

                # KDA stats
                curr['~03total_kills'] += participant['stats']['kills']
                curr['~04total_deaths'] += participant['stats']['deaths']
                curr['~05total_assists'] += participant['stats']['assists']

                # Damage dealt to champions stats
                curr["~06total_totalDamageDealtToChampions"] += participant['stats']['totalDamageDealtToChampions']
                curr["~07total_magicDamageDealtToChampions"] += participant['stats']['magicDamageDealtToChampions']
                curr["~08total_physicalDamageDealtToChampions"] += participant['stats']['physicalDamageDealtToChampions']
                curr["~09total_trueDamageDealtToChampions"] += participant['stats']['trueDamageDealtToChampions']

                # Damage dealt
                curr["~10total_totalDamageDealt"] += participant['stats']['totalDamageDealt']
                curr["~11total_magicDamageDealt"] += participant['stats']['magicDamageDealt']
                curr["~12total_physicalDamageDealt"] += participant['stats']['physicalDamageDealt']
                curr["~13total_trueDamageDealt"] += participant['stats']['trueDamageDealt']

                curr["~14total_damageDealtToObjectives"] += participant['stats']['damageDealtToObjectives']
                curr["~15total_damageDealtToTurrets"] += participant['stats']['damageDealtToTurrets']

                # Damage taken and healed
                curr["~16total_totalHeal"] += participant['stats']['totalHeal']
                curr["~17total_totalDamageTaken"] += participant['stats']['totalDamageTaken']
                curr["~18total_magicalDamageTaken"] += participant['stats']['magicalDamageTaken']
                curr["~19total_physicalDamageTaken"] += participant['stats']['physicalDamageTaken']
                curr["~20total_trueDamageTaken"] += participant['stats']['trueDamageTaken']
                curr["~21total_damageSelfMitigated"] += participant['stats']['damageSelfMitigated']

                # Income
                curr["~22total_goldEarned"] += participant['stats']['goldEarned']
                curr["~23total_goldSpent"] += participant['stats']['goldSpent']

                # Vision
                curr["~24total_visionScore"] += participant['stats']['visionScore']
                curr["~25total_visionWardsBoughtInGame"] += participant['stats']['visionWardsBoughtInGame']
                curr["~26total_wardsPlaced"] += participant['stats']['wardsPlaced']
                curr["~27total_wardsKilled"] += participant['stats']['wardsKilled']

                # Neutral monsters and minions
                curr["~28total_totalMinionsKilled"] += participant['stats']['totalMinionsKilled']
                curr["~29total_neutralMinionsKilled"] += participant['stats']['neutralMinionsKilled']
                curr["~30total_neutralMinionsKilledTeamJungle"] += participant['stats']['neutralMinionsKilledTeamJungle']
                curr["~31total_neutralMinionsKilledEnemyJungle"] += participant['stats']['neutralMinionsKilledEnemyJungle']

                # Firsts
                # Getting current team struct for the participant
                curr_team = filter(lambda x: x['teamId'] == participant['teamId'], games[game]['teams'])[0]
                curr["~32total_firstBloodKill"] += int(curr_team['firstBlood'] and participant['stats']['firstBloodKill'])
                curr["~33total_firstBloodAssist"] += int(curr_team['firstBlood'] and participant['stats']['firstBloodAssist'])  # For some reason this field is unused.
                curr["~34total_firstTowerKill"] += int(curr_team['firstTower'] and participant['stats']['firstTowerKill'])
                curr["~35total_firstTowerAssist"] += int(curr_team['firstTower'] and participant['stats']['firstTowerAssist'])
                curr["~36total_firstInhibitorKill"] += int(curr_team['firstInhibitor'] and participant['stats']['firstInhibitorKill'])
                curr["~37total_firstInhibitorAssist"] += int(curr_team['firstInhibitor'] and participant['stats']['firstInhibitorAssist'])

                # Objective kills and Crowd Control
                curr["~38total_turretKills"] += participant['stats']['turretKills']
                curr["~39total_inhibitorKills"] += participant['stats']['inhibitorKills']

                curr["~40total_timeCCingOthers"] += participant['stats']['timeCCingOthers']  # is actually cc score
                curr["~41total_totalTimeCrowdControlDealt"] += participant['stats']['totalTimeCrowdControlDealt']

                ################################### Average stats ###############################
                # Win/Lose stats
                curr["~42average_won"] = (1.0 * curr["~42average_won"] * prev_games_played + int(participant['stats']['win'])) / curr["~00games_played"]
                curr["~43average_lost"] = (1.0 * curr["~43average_lost"] * prev_games_played + int(not participant['stats']['win'])) / curr["~00games_played"]

                # KDA stats
                curr["~44average_kills"] = (1.0 * curr["~44average_kills"] * prev_games_played + participant['stats']['kills']) / curr["~00games_played"]
                curr["~45average_deaths"] = (1.0 * curr["~45average_deaths"] * prev_games_played + participant['stats']['deaths']) / curr["~00games_played"]
                curr["~46average_assists"] = (1.0 * curr["~46average_assists"] * prev_games_played + participant['stats']['assists']) / curr["~00games_played"]

                # Damage dealt to champions stats
                curr["~47average_totalDamageDealtToChampions"] = (1.0 * curr["~47average_totalDamageDealtToChampions"] * prev_games_played + participant['stats']['totalDamageDealtToChampions']) / curr["~00games_played"]
                curr["~48average_magicDamageDealtToChampions"] = (1.0 * curr["~48average_magicDamageDealtToChampions"] * prev_games_played + participant['stats']['magicDamageDealtToChampions']) / curr["~00games_played"]
                curr["~49average_physicalDamageDealtToChampions"] = (1.0 * curr["~49average_physicalDamageDealtToChampions"] * prev_games_played + participant['stats']['physicalDamageDealtToChampions']) / curr["~00games_played"]
                curr["~50average_trueDamageDealtToChampions"] = (1.0 * curr["~50average_trueDamageDealtToChampions"] * prev_games_played + participant['stats']['trueDamageDealtToChampions']) / curr["~00games_played"]

                # Damage dealt
                curr["~51average_totalDamageDealt"] = (1.0 * curr["~51average_totalDamageDealt"] * prev_games_played + participant['stats']['totalDamageDealt']) / curr["~00games_played"]
                curr["~52average_magicDamageDealt"] = (1.0 * curr["~52average_magicDamageDealt"] * prev_games_played + participant['stats']['magicDamageDealt']) / curr["~00games_played"]
                curr["~53average_physicalDamageDealt"] = (1.0 * curr["~53average_physicalDamageDealt"] * prev_games_played + participant['stats']['physicalDamageDealt']) / curr["~00games_played"]
                curr["~54average_trueDamageDealt"] = (1.0 * curr["~54average_trueDamageDealt"] * prev_games_played + participant['stats']['trueDamageDealt']) / curr["~00games_played"]

                curr["~55average_damageDealtToObjectives"] = (1.0 * curr["~44average_kills"] * prev_games_played + participant['stats']['damageDealtToObjectives']) / curr["~00games_played"]
                curr["~56average_damageDealtToTurrets"] = (1.0 * curr["~44average_kills"] * prev_games_played + participant['stats']['damageDealtToTurrets']) / curr["~00games_played"]

                # Damage taken and healed
                curr["~57average_totalHeal"] = (1.0 * curr["~57average_totalHeal"] * prev_games_played + participant['stats']['totalHeal']) / curr["~00games_played"]
                curr["~58average_totalDamageTaken"] = (1.0 * curr["~58average_totalDamageTaken"] * prev_games_played + participant['stats']['totalDamageTaken']) / curr["~00games_played"]
                curr["~59average_magicalDamageTaken"] = (1.0 * curr["~59average_magicalDamageTaken"] * prev_games_played + participant['stats']['magicalDamageTaken']) / curr["~00games_played"]
                curr["~60average_physicalDamageTaken"] = (1.0 * curr["~60average_physicalDamageTaken"] * prev_games_played + participant['stats']['physicalDamageTaken']) / curr["~00games_played"]
                curr["~61average_trueDamageTaken"] = (1.0 * curr["~61average_trueDamageTaken"] * prev_games_played + participant['stats']['trueDamageTaken']) / curr["~00games_played"]
                curr["~62average_damageSelfMitigated"] = (1.0 * curr["~62average_damageSelfMitigated"] * prev_games_played + participant['stats']['damageSelfMitigated']) / curr["~00games_played"]

                # Income
                curr["~63average_goldEarned"] = (1.0 * curr["~63average_goldEarned"] * prev_games_played + participant['stats']['kills']) / curr["~00games_played"]
                curr["~64average_goldSpent"] = (1.0 * curr["~64average_goldSpent"] * prev_games_played + participant['stats']['kills']) / curr["~00games_played"]

                # Vision
                curr["~65average_visionScore"] = (1.0 * curr["~65average_visionScore"] * prev_games_played + participant['stats']['visionScore']) / curr["~00games_played"]
                curr["~66average_visionWardsBoughtInGame"] = (1.0 * curr["~66average_visionWardsBoughtInGame"] * prev_games_played + participant['stats']['visionWardsBoughtInGame']) / curr["~00games_played"]
                curr["~67average_wardsPlaced"] = (1.0 * curr["~67average_wardsPlaced"] * prev_games_played + participant['stats']['wardsPlaced']) / curr["~00games_played"]
                curr["~68average_wardsKilled"] = (1.0 * curr["~68average_wardsKilled"] * prev_games_played + participant['stats']['wardsKilled']) / curr["~00games_played"]

                # Neutral monsters and minions
                curr["~69average_totalMinionsKilled"] = (1.0 * curr["~69average_totalMinionsKilled"] * prev_games_played + participant['stats']['totalMinionsKilled']) / curr["~00games_played"]
                curr["~70average_neutralMinionsKilled"] = (1.0 * curr["~70average_neutralMinionsKilled"] * prev_games_played + participant['stats']['neutralMinionsKilled']) / curr["~00games_played"]
                curr["~71average_neutralMinionsKilledTeamJungle"] = (1.0 * curr["~71average_neutralMinionsKilledTeamJungle"] * prev_games_played + participant['stats']['neutralMinionsKilledTeamJungle']) / curr["~00games_played"]
                curr["~72average_neutralMinionsKilledEnemyJungle"] = (1.0 * curr["~72average_neutralMinionsKilledEnemyJungle"] * prev_games_played + participant['stats']['neutralMinionsKilledEnemyJungle']) / curr["~00games_played"]

                # Firsts
                curr["~73average_firstBloodKill"] = (1.0 * curr["~73average_firstBloodKill"] * prev_games_played + int(curr_team['firstBlood'] and participant['stats']['firstBloodKill'])) / curr["~00games_played"]
                curr["~74average_firstBloodAssist"] = (1.0 * curr["~74average_firstBloodAssist"] * prev_games_played +int(curr_team['firstBlood'] and participant['stats']['firstBloodAssist'])) / curr["~00games_played"]
                curr["~75average_firstTowerKill"] = (1.0 * curr["~75average_firstTowerKill"] * prev_games_played + int(curr_team['firstTower'] and participant['stats']['firstTowerKill'])) / curr["~00games_played"]
                curr["~76average_firstTowerAssist"] = (1.0 * curr["~76average_firstTowerAssist"] * prev_games_played + int(curr_team['firstTower'] and participant['stats']['firstTowerAssist'])) / curr["~00games_played"]
                curr["~77average_firstInhibitorKill"] = (1.0 * curr["~77average_firstInhibitorKill"] * prev_games_played + int(curr_team['firstInhibitor'] and participant['stats']['firstInhibitorKill'])) / curr["~00games_played"]
                curr["~78average_firstInhibitorAssist"] = (1.0 * curr["~78average_firstInhibitorAssist"] * prev_games_played + int(curr_team['firstInhibitor'] and participant['stats']['firstInhibitorAssist'])) / curr["~00games_played"]

                # Objective kills and Crowd Control
                curr["~79average_turretKills"] = (1.0 * curr["~79average_turretKills"] * prev_games_played + participant['stats']['turretKills']) / curr["~00games_played"]
                curr["~80average_inhibitorKills"] = (1.0 * curr["~80average_inhibitorKills"] * prev_games_played + participant['stats']['inhibitorKills']) / curr["~00games_played"]

                curr["~81average_timeCCingOthers"] = (1.0 * curr["~81average_timeCCingOthers"] * prev_games_played + participant['stats']['timeCCingOthers']) / curr["~00games_played"]  # is actually cc score
                curr["~82average_totalTimeCrowdControlDealt"] = (1.0 * curr["~82average_totalTimeCrowdControlDealt"] * prev_games_played + participant['stats']['totalTimeCrowdControlDealt']) / curr["~00games_played"]

        print('[DEBUG] Games checked: ' + str(games_checked))

    if tier == 'all':
        write_json_dict_to_disk(lol_champs_average_stats, 'all_tiers_champs_average_stats.json')
    else:
        write_json_dict_to_disk(lol_champs_average_stats, tier + ' games/' + tier + '_champs_average_stats.json')


# Given tier name, goes to the tier folder, grabs the games files per every player and inserts every game number
# into a list of game numbers for the tier, keeping it without duplications.
# Tier names are: 'bronze', 'silver', 'gold', 'platinum', 'diamond', 'master', and 'challenger'
def filter_game_numbers(tier):
    print(tier)

    # arguments check
    if tier not in ['bronze', 'silver', 'gold', 'platinum', 'diamond', 'master', 'challenger']:
        print('[ERROR] Unexpected tier name.\n\tTier name should be one of the following:\n' +
              '\tbronze, silver, gold, platinum, diamond, master, challenger\n')
        return

    # print('Function work')

    # Getting the filenames in the tier directory
    filenames = os.listdir(tier + ' games')
    # print(filenames)

    # Open the filtered JSON file
    unique_game_numbers = get_json_from_file(tier + ' games/' + tier + '_game_numbers_unique.json')
    # print(unique_game_numbers)

    # Traverse over the filenames
    for filename in filenames:
        # assuming that player filenames start with number
        if ord(filename[0]) < 48 or ord(filename[0]) > 57:
            pass
        else:
            print(filename)
            # Open a JSON file
            filename_json = get_json_from_file(tier + ' games/' + filename)
            # print(filename_json)

            # Traverse over the games in the filename and add it to the unique game numbers if it is unique
            for match in filename_json['matches']:
                # print(match['gameId'])
                unique_game_numbers[match['gameId']] = match['gameId']

    # Writing the unique JSON to disk
    # print(unique_game_numbers)
    write_json_dict_to_disk(unique_game_numbers, tier + ' games/' + tier + '_game_numbers_unique.json')


# Given a tier, download all of the games within the unique game numbers list for that tier.
# Download will be done by respecting rate limits, meaning the amount of games downloaded per second
# will not exceed the limit of MATCHES_MATCHID_RATE_LIMIT, that is set for 40 requests per second
# for that aplication, conforming to riots API rate limit of 500 requests per 10 seconds.
def download_games(tier):
    print(tier)

    # arguments check
    if tier not in ['bronze', 'silver', 'gold', 'platinum', 'diamond', 'master', 'challenger']:
        print('[ERROR] Unexpected tier name.\n\tTier name should be one of the following:\n' +
              '\tbronze, silver, gold, platinum, diamond, master, challenger\n')
        return

    # Initializing the matches dictionary
    games = {}

    # Setting the number of requests per second made to 0. When they exceed the limit, wait 1 second and refresh it.
    requests_per_second = 0

    # Setting the number of requests per minute made to 0. When they exceed the limit, wait 1 minute and refresh it.
    requests_per_minute = 0

    # Query params:
    api_base_query = 'api.riotgames.com/lol/match/v3/matches/'

    # Open the filtered JSON file
    unique_game_numbers = get_json_from_file(tier + ' games/' + tier + '_game_numbers_unique.json')

    for gameID in unique_game_numbers:
        print('[DEBUG]' + str(unique_game_numbers[gameID]))

        if requests_per_second == REQUESTS_PER_SECOND_LIMIT:
            print('[DEBUG] sleeping on requests per second...')
            time.sleep(1)
            requests_per_second = 0
        else:
            pass

        if requests_per_minute == REQUESTS_PER_MINUTE_LIMIT:
            print('[DEBUG] sleeping on requests per minute...')
            time.sleep(60)
            requests_per_minute = 0
        else:
            pass

        # Retrieving a JSON object for champions from the api
        js_obj = json.load(urllib2.urlopen(API_METHOD +
                                           API_REGION +
                                           api_base_query +
                                           str(unique_game_numbers[gameID]) +
                                           '?api_key=' + API_KEY))

        # Inserting the newly retrieved match into the matches list
        games[gameID] = js_obj

        # Updating the requests per second counter
        requests_per_second += 1

        # Updating the requests per second counter
        requests_per_minute += 1

    write_json_dict_to_disk(games, tier + ' games/' + tier + '_games.json')

# Given a tier, filter its remaked games
def filter_remake_games(tier):
    print('[DEBUG] ' + tier)

    # Opening games json for this tier
    games = get_json_from_file(tier + ' games/' + tier + '_games.json')

    # Initialize filtered games
    filtered_games = {}

    # Filtering
    for game in games:
        if games[game]['gameDuration'] > 330:
            filtered_games[game] = games[game]

    write_json_dict_to_disk(filtered_games, tier + ' games/' + tier + '_remake_filtered_games.json')

# ======================================================================================================================
# ======================================================================================================================
# ====================================================== Functions =====================================================
# ======================================================================================================================
# ======================================================================================================================


# ======================================================================================================================
# ======================================================================================================================
# ======================================================== Main ========================================================
# ======================================================================================================================
# ======================================================================================================================

# Create an empty champion average stats JSON file from champions list file or api
# create_empty_champion_average_stats_from_file('lol_champs_list.json')
# create_empty_champion_average_stats_from_api()

# # Filter and remove duplicate game numbers for each division
# filter_game_numbers('bronze')
# filter_game_numbers('silver')
# filter_game_numbers('gold')
# filter_game_numbers('platinum')
# filter_game_numbers('diamond')
# filter_game_numbers('master')
# filter_game_numbers('challenger')

# Download the games from each division
# download_games('bronze')
# download_games('silver')
# download_games('gold')
# download_games('platinum')
# download_games('diamond')
# download_games('master')
# download_games('challenger')

# Filter remaked games
# filter_remake_games('bronze')
# filter_remake_games('silver')
# filter_remake_games('gold')
# filter_remake_games('platinum')
# filter_remake_games('diamond')
# filter_remake_games('master')
# filter_remake_games('challenger')

# # Fill the JSON file with data about the champions and save it as a new JSON file
# fill_champions_avrage_stats('bronze')
# fill_champions_avrage_stats('silver')
# fill_champions_avrage_stats('gold')
# fill_champions_avrage_stats('platinum')
# fill_champions_avrage_stats('diamond')
# fill_champions_avrage_stats('master')
# fill_champions_avrage_stats('challenger')
# fill_champions_avrage_stats('all')

# For debug purposes
print("[DEBUG] Program executed normally")

# ======================================================================================================================
# ======================================================================================================================
# ======================================================== Main ========================================================
# ======================================================================================================================
# ======================================================================================================================