# A GUI for the project

import sys
sys.path.insert(0, r'D:/sentdex/fightorflight/ML_Implementation')

from Tkinter import *
import winner_predictor as wp

TIERS = ['All', 'Challenger', 'Master', 'Diamond', 'Platinum', 'Gold', 'Silver', 'Bronze']
COMPARISON_TIER = 'none'
CLASSIFIER = 'none'

def show_data():
    global COMPARISON_TIER
    global CLASSIFIER

    txt.delete(0.0, 'end')
    gameID = ent.get()
    tier = TIERS[var_chk.get()]

    if tier == COMPARISON_TIER:
        print("[DEBUG] No need to train again")
    else:
        print("[DEBUG] Training with " + tier + " tier data")
        COMPARISON_TIER = tier
        CLASSIFIER = wp.train_by_tier("../ML_Implementation/" + tier.lower() + '_games_csv.txt')

    if CLASSIFIER == 'none':
        print("[DEBUG] Error")
        sentence = "Game ID: " + gameID + "\nTier: " + tier
        sentence += "\nCould not predict outcome"
        txt.insert(0.0, sentence)
    else:
        prediction, actual_outcome = wp.predict_by_gameId(CLASSIFIER, gameID)
        sentence = "Game ID: " + gameID + "\nTier: " + tier
        sentence += "\nPredicted winner: Team " + str(prediction) + "\nActual winner: Team " + str(actual_outcome)
        txt.insert(0.0, sentence)

# Actual program
root = Tk()
root.title("FightOrFlight")
root.geometry("500x340")

# ############ The title ###########
frame1 = Frame(root)
frame1.pack(pady=10)

headline = Label(frame1, text="Game outcome prediction tool")
headline.pack()
# ############ The title ###########

# ############ The rest ############
frame2 = Frame(root)
frame2.pack()

#       ############ The input ###########
frame2_1 = Frame(frame2)
frame2_1.pack(side=LEFT, padx=5)

l1 = Label(frame2_1, text="Game ID: ")
l2 = Label(frame2_1, text="Comparison tier: ")
l1.grid(row=0, column=0, sticky=W)
l2.grid(row=1)

ent = Entry(frame2_1)
ent.insert(0, 2018802753)
ent.grid(row=0, column=1, sticky=W)

var_chk = IntVar()
rd0 = Radiobutton(frame2_1, text="All Tiers", variable=var_chk, value=0)
rd1 = Radiobutton(frame2_1, text="Challenger", variable=var_chk, value=1)
rd2 = Radiobutton(frame2_1, text="Master", variable=var_chk, value=2)
rd3 = Radiobutton(frame2_1, text="Diamond", variable=var_chk, value=3)
rd4 = Radiobutton(frame2_1, text="Platinum", variable=var_chk, value=4)
rd5 = Radiobutton(frame2_1, text="Gold", variable=var_chk, value=5)
rd6 = Radiobutton(frame2_1, text="Silver", variable=var_chk, value=6)
rd7 = Radiobutton(frame2_1, text="Bronze", variable=var_chk, value=7)

rd0.grid(row=2, column=0, sticky=W)
rd1.grid(row=3, column=0, sticky=W)
rd2.grid(row=4, column=0, sticky=W)
rd3.grid(row=5, column=0, sticky=W)
rd4.grid(row=6, column=0, sticky=W)
rd5.grid(row=7, column=0, sticky=W)
rd6.grid(row=8, column=0, sticky=W)
rd7.grid(row=9, column=0, sticky=W)
#       ############ The input ###########

#       ############ The result ##########
frame2_2 = Frame(frame2)
frame2_2.pack(side=RIGHT, padx=5)

txt = Text(frame2_2, width=25, height=15, wrap=WORD)
txt.grid(row=0, column=2)
#       ############ The result ##########

# ############ The rest ############

# ########### The submit ###########
frame3 = Frame(root)
frame3.pack(pady=10)

btn = Button(frame3, text="SUBMIT", bg="purple", fg="white", command=show_data)
btn.grid(row=0, column=1, columnspan=2)
# ########### The submit ###########

root.mainloop()
# Actual program
